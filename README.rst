==========
Spyder-vcs
==========

An atom-style integrations for VCS in Spyder (such as git).
Here I develop and track bugs for `spyder#13562 <https://github.com/spyder-ide/spyder/pull/13562>`_.


The code here sightly differs from the PR one because plugin loading
(from the spyder-py3-dev/plugin directory) is broken for the new plugin API,
therefore make the plugin working requires some workarounds.


TODO
====

The TODO list is in the issue #1.


Disclaimer
==========

This repository is not affiliated to the Spyder project and the Spyder Dev Team.