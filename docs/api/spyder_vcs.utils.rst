spyder\_vcs.utils package
=========================

Submodules
----------

spyder\_vcs.utils.api module
----------------------------

.. automodule:: spyder_vcs.utils.api
   :members:
   :undoc-members:
   :show-inheritance:

spyder\_vcs.utils.backend module
--------------------------------

.. automodule:: spyder_vcs.utils.backend
   :members:
   :undoc-members:
   :show-inheritance:

spyder\_vcs.utils.errors module
-------------------------------

.. automodule:: spyder_vcs.utils.errors
   :members:
   :undoc-members:
   :show-inheritance:

spyder\_vcs.utils.mixins module
-------------------------------

.. automodule:: spyder_vcs.utils.mixins
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: spyder_vcs.utils
   :members:
   :undoc-members:
   :show-inheritance:
