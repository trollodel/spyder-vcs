spyder\_vcs.widgets package
===========================

Submodules
----------

spyder\_vcs.widgets.common module
---------------------------------

.. automodule:: spyder_vcs.widgets.common
   :members:
   :undoc-members:
   :show-inheritance:

spyder\_vcs.widgets.vcsgui module
---------------------------------

.. automodule:: spyder_vcs.widgets.vcsgui
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: spyder_vcs.widgets
   :members:
   :undoc-members:
   :show-inheritance:
