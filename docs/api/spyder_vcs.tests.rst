spyder\_vcs.tests package
=========================

Submodules
----------

spyder\_vcs.tests.test\_plugin module
-------------------------------------

.. automodule:: spyder_vcs.tests.test_plugin
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: spyder_vcs.tests
   :members:
   :undoc-members:
   :show-inheritance:
