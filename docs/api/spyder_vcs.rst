spyder\_vcs package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   spyder_vcs.tests
   spyder_vcs.utils
   spyder_vcs.widgets

Submodules
----------

spyder\_vcs.plugin module
-------------------------

.. automodule:: spyder_vcs.plugin
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: spyder_vcs
   :members:
   :undoc-members:
   :show-inheritance:
