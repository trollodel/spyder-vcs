Welcome to spyder-vcs's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
    
   api/modules

Modules
=======

.. .. autosummary::
..    :toctree: modules
..    :recursive:
.. 
..    spyder_vcs



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
